<?php
    if(!class_exists('WM_Custom_Post_Type_Elements')) {
        class WM_Custom_Post_Type_Elements{
            public function __construct() {
                add_action('init', array($this, 'custom_post_type_member'));
            }
            
            public function custom_post_type_member() {
                $labels = array(
                    'name' => 'Teasers',
                    'singular_name' => 'Teaser',
                    'add_new' => 'Add Teaser',
                    'all_items' => 'All Teasers',
                    'add_new_item' => 'Add Teaser',
                    'edit_item' => 'Edit Teaser',
                    'new_item' => 'New Teaser',
                    'view_item' => 'View Teaser',
                    'search_item' => 'Search Teaser',
                    'not_found' => 'No Teasers found',
                    'not_found_in_trash' => 'No Teasers found in trash',
                    'parent_item_colon' => 'Parent Teaser',
                );
                $args = array(
                    'labels' => $labels,
                    'public' => true,
                    'has_archive' => false,
                    'publicly_queryable' => true,
                    'query_var' => true,
                    'rewrite' => true,
                    'capability_type' => 'post',
                    'hierarchical' => false,
                    'menu_position' => 5,
                    'exclude_from_search' => false,
                    'menu_icon' => 'dashicons-welcome-add-page'
                );
                register_post_type('teasers', $args);


                $labels2 = array(
                    'name' => 'Elements',
                    'singular_name' => 'Element',
                    'add_new' => 'Add Element',
                    'all_items' => 'All Elements',
                    'add_new_item' => 'Add Elements',
                    'edit_item' => 'Edit Element',
                    'new_item' => 'New Element',
                    'view_item' => 'View Element',
                    'search_item' => 'Search Element',
                    'not_found' => 'No Element found',
                    'not_found_in_trash' => 'No Elements found in trash',
                    'parent_item_colon' => 'Parent Element',
                );
                $args2 = array(
                    'labels' => $labels2,
                    'public' => true,
                    'has_archive' => false,
                    'publicly_queryable' => true,
                    'query_var' => true,
                    'rewrite' => true,
                    'capability_type' => 'post',
                    'hierarchical' => false,
                    'menu_position' => 5,
                    'exclude_from_search' => false,
                    'menu_icon' => 'dashicons-welcome-add-page'
                );
                register_post_type('elements', $args2);
            }
        }
    }

