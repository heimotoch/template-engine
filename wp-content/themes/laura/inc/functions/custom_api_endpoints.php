<?php

add_action('rest_api_init', function() {
    
    register_rest_route('wl/v1', 'fonts', array(
		'methods' => 'GET',
		'callback' => 'get_fonts'
	));
});

function get_fonts() {

    $fonts = get_field('font', 'option');

    $fontGroup = array();

    foreach($fonts as $font){
        $name = $font["font_name"];
        $link = $font["font_link"];
        $size =  $font["font_size"];
        $usage = $font["font_usage"];

        $font = ['name' => $name, 'link' => $link, 'size' => $size, 'usage' => $usage];

        array_push($fontGroup, $font);

        wp_enqueue_style($name, $link, array(), null, 'all');
    }

	return $fontGroup;
}