<?php

    $fonts = get_field('font', 'option');
    $fontsArr = array();

    $allFonts =  get_field('fonts', 'option');

    foreach($fonts as $font){
        $name = $font["fontname"];
        
        foreach($allFonts as $allFont){
            if($name == $allFont['font_name']) {
                $link = $allFont["font_link"];

                var_dump($allFont["font_usage"]);

                wp_enqueue_style($name, $link, array(), null, 'all');

            }
        }
    }
