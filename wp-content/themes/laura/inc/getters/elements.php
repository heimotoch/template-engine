<?php


$elements = new WP_Query([
    'post_type' => 'elements',
	'posts_per_page' => -1
]);

if ($elements->have_posts()) {
	while ( $elements->have_posts() ) {
        $elements->the_post();

        $position = get_field('position');
        $top = $position['position_vertical'];
        $positionTop = $top === 'top' ? 0 : ($top == 'center' ? 50 : 100);
        $left = $position['position_horizontal'];
        $positionLeft = $left == 'left' ? 0 : ($left == 'center' ? 50 : 100);

        $editor = get_the_content();



        $arr = array(
            'type' => 'element',
            'position' => [
                'top' =>  $positionTop,
                'left' =>   $positionLeft,
            ],
            'width' => get_field('width'),
            'margin_top' => get_field('margin_top'),
            'margin_left' => get_field('margin_left'),
            'fade_out_after' => get_field('fade_out_after'),
            'editor' => $editor,
        );

        array_push($modules, $arr);
	}
	wp_reset_postdata();
};
