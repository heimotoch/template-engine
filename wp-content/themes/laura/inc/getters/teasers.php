<?php

$teasers = new WP_Query([
    'post_type' => 'teasers',
    'posts_per_page' => -1
]);

if ($teasers->have_posts()) {
    while ( $teasers->have_posts() ) {
        $teasers->the_post();
        $module_name = get_row_layout();

        $editor = get_the_content();

        $align_to_grid = get_field('align_to_grid');

        $arr = array(
            'type' => 'teaser',
            'height' => get_field('height'),
            'align_to_grid' => $align_to_grid,
            'background' => get_field('background'),
            'corner_radius' => get_field('corner_radius'),
            'editor' => $editor,
            'padding' => get_field('padding'),
            'margin' => get_field('margin'),

        );

        if($align_to_grid) {
            $arr['grid'] = get_field('grid');
            $arr['width'] = false;
        } else {
            $arr['width'] = get_field('width') . 'vw';
        }

        array_push($modules, $arr);
    }
wp_reset_postdata();
};