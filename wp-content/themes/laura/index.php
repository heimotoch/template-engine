<?php
$context = Timber::context();
$modules = array();
$all_images = array();

// require_once(__DIR__ . '/inc/getters/elements.php');
// require_once(__DIR__ . '/inc/getters/teasers.php');

require_once(__DIR__ . '/inc/functions/register_fonts.php');





function getMaxGridWidth() {
    return get_field('max_grid_width', 'option');
}

function getBackgroundColor() {
    return get_field('background_color', 'option');
}






if(have_rows('modules')) {
    while(have_rows('modules')) {
        the_row();
        $module_name = get_row_layout();
        $module = array();

        if($module_name == 'clock') {
            $module = array(
                'type'  						=> $module_name,
                'font_as'  						=> get_sub_field('font_as'),

            );

            $align_to_grid = get_sub_field('align_to_grid');
            if($align_to_grid) {
                $module['grid'] = get_sub_field('grid');
                $module['width'] = false;
            } else {
                $module['width'] = get_sub_field('width') . 'vw';
            }
        } else if($module_name == 'line') {
            $module = array(
                'type'  						=> $module_name,
                'id' => get_row_index(),
                'color' => get_sub_field('color'),
                'height' => get_sub_field('height'),
                'margin_top' => get_sub_field('margin_top'),
            );

            $align_to_grid = get_sub_field('align_to_grid');
            if($align_to_grid) {
                $module['grid'] = get_sub_field('grid');
                $module['width'] = false;
            } else {
                $module['width'] = get_sub_field('width') . 'vw';
            }
        } else if($module_name == 'images') {
            $images = array();

            if(have_rows('image')) {
                while(have_rows('image')) {
                    the_row();

                    $image = [
                        'img' => get_sub_field('image_file'),
                        'bump_up' => get_sub_field('bump_up')
                    ];

                    if($align_to_grid) {
                        $image['grid'] = get_sub_field('grid');
                        $image['width'] = false;
                    } else {
                        $image['width'] = get_sub_field('width') . 'vw';
                    }

                    array_push($all_images, get_sub_field('image_file'));
                    array_push($images, $image);
                }
            }

            $module = array(
                'type'  						=> $module_name,
                'id' => get_row_index(),
                'margin_top' => get_sub_field('margin_top'),
                'extra_margin_top' => get_sub_field('extra_margin_top'),
                'images' => $images,
            );
        } else if($module_name == 'images_masonry') {
            $module = array(
                'type'  						=> $module_name,
                'id' => get_row_index(),
                'margin_top' => get_sub_field('margin_top'),
                'columns' => get_sub_field('columns'),
                'images' => get_sub_field('images'),
            );
        } else if($module_name == 'window_module') {


            $module = array(
                'type' => $module_name,
                'id' => get_row_index(),
                'margin_top' => get_sub_field('margin_top'),
                'height' => get_sub_field('height'),
                'image' => get_sub_field('image'),
            );
        } else if($module_name == 'scroll_counter') {
            $module = array(
                'type'  						=> $module_name,
                'font_as'  						=>  get_sub_field('font_as'),
            );

        } else if($module_name == 'space_module') {
            $module = array(
                'type'  						=> $module_name,
                'height'  						=>  get_sub_field('height'),
            );
        } else if($module_name == 'logo') {
            $module = array(
                'type'  						=> $module_name,
                'is_logo_image'  						=>  get_sub_field('is_logo_image'),
                'text'  						=>  get_sub_field('text'),
                'font_as'  						=>  get_sub_field('font_as'),
                'image'  						=>  get_sub_field('image'),
                'position'  						=>  get_sub_field('position'),
                'width'  						=>  get_sub_field('width'),
                'margin'  						=>  get_sub_field('margin'),
            );
        } else if($module_name == 'heading') {
            $module = array(
                'type'  						=> $module_name,
                'id' => get_row_index(),
                'text' => get_sub_field('text'),
                'text_align' => get_sub_field('text_align'),
                'margin_top' => get_sub_field('margin_top'),
                'font_as'  						=>  get_sub_field('font_as'),

            );

            $align_to_grid = get_sub_field('align_to_grid');
            if($align_to_grid) {
                $module['grid'] = get_sub_field('grid');
                $module['width'] = false;
            } else {
                $module['width'] = get_sub_field('width') . 'vw';
            }
        } else if($module_name == 'table') {
            $module = array(
                'type'  						=> $module_name,
                'id' => get_row_index(),
                'columns_count' => get_sub_field('columns_count'),
                'items' => get_sub_field('items'),
                'margin_top' => get_sub_field('margin_top'),
                'font_as'  						=>  get_sub_field('font_as'),
            );

            $align_to_grid = get_sub_field('align_to_grid');
            if($align_to_grid) {
                $module['grid'] = get_sub_field('grid');
                $module['width'] = false;
            } else {
                $module['width'] = get_sub_field('width') . 'vw';
            }
        }

        array_push($modules, $module);
    }
}




$context['modules'] = $modules;
$context['images'] = $all_images;

$context['max_grid_width'] = getMaxGridWidth();
$context['background_color'] = getBackgroundColor();




Timber::render( 'index.twig', $context ); 