<?php /* Template Name: Options */ 

$context = Timber::context();
$options = array();


function registerAllFonts() {
    $fonts = get_field('fonts', 'option');

    foreach($fonts as $font){
        $name = $font["font_name"];
        $link = $font["font_link"];
        wp_enqueue_style($name, $link, array(), null, 'all');
    }

    return $fonts;
}
registerAllFonts();



$context['fonts'] = registerAllFonts();

Timber::render( 'options.twig', $context ); 


?>