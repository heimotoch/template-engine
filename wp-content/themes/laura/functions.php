<?php
// add_filter('use_block_editor_for_post', '__return_false', 10);
// add_filter('use_block_editor_for_post_type', '__return_false', 10);

require_once(__DIR__ . '/inc/classes/WmCustomPostTypeElements.php');
require_once(__DIR__ . '/inc/functions/enqueue_files.php');
require_once(__DIR__ . '/inc/functions/custom_api_endpoints.php');

add_theme_support( 'menus' );
$WmCustomPostTypeElements = new WM_Custom_Post_Type_Elements();

// add_action('admin_init', 'remove_textarea');

function remove_textarea() {
        remove_post_type_support( 'post', 'editor' );
        remove_post_type_support( 'page', 'editor' );
}


if( function_exists('acf_add_options_page') ) {	
	acf_add_options_page();	
}


/* functions.php */
add_filter( 'timber_context', 'mytheme_timber_context'  );
function mytheme_timber_context( $context ) {
    $context['options'] = get_fields('option');
    return $context;
}













function acf_load_font_name_field_choices( $field ) {
    $field['choices'] = array();

    if( have_rows('fonts', 'option') ) {
        while( have_rows('fonts', 'option') ) {
            the_row();          

            $value = get_sub_field('font_name');
            $label = get_sub_field('font_name');
            
            $field['choices'][ $value ] = $label;            
        }       
    }
    return $field;   
}
add_filter('acf/load_field/name=fontname', 'acf_load_font_name_field_choices');
