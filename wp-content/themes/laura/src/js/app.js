import lazy from './lazy.js'
import slider from './slider.js'
import clock from './clock.js'
import macyFunc from './macyFunc.js'



// import gsap from 'gsap'


const lazyLoadInstance = new LazyLoad();
lazy();
slider();
clock();
macyFunc();




const defineGridGap = () => {
    const gridWidth = document.querySelector('.grid-susy').offsetWidth
    const gridGapDesktop = gridWidth / 24 * (1 - 0.8055);
    const gridGapTablet = (gridWidth / 12) * (0.12);
    const gridGapMobile = gridWidth / 12 * (0.2);

    document.documentElement.style.setProperty("--gridGapDesktop", gridGapDesktop);
    document.documentElement.style.setProperty("--gridGapTablet", gridGapTablet);
    document.documentElement.style.setProperty("--gridGapMobile", gridGapMobile);
}

defineGridGap()




const scrollCounter = document.querySelector('.scroll_counter')
window.addEventListener('scroll', () => {
    defineGridGap();

        if(scrollCounter) {
            scrollCounter.children[0].innerHTML = window.pageYOffset
        }
    }
)




