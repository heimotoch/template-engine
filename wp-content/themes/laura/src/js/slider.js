import gsap from 'gsap'
import * as Hammer from 'hammerjs';

const slider = () => {
    const body = document.querySelector('body')
    const box = document.querySelector('.box')
    const imgs = document.querySelectorAll('.images__image')
    const nextBtn  = document.querySelector('.box__next')
    const prevBtn  = document.querySelector('.box__prev')
    const closeBtn = document.querySelector('.box__close')


    class Slider {
        constructor() {
            this.isOpen = false,
            this.current = null

            window.addEventListener('keydown', (event) => {
                const key = event.key; // "ArrowRight", "ArrowLeft", "ArrowUp", or "ArrowDown"
                if(key == 'ArrowLeft') {
                    this.prevSlide()
                } else if (key == 'ArrowRight') {
                    this.nextSlide()
                } else if (key == 'Escape') {
                    this.closeBox()
                }
            });
        }

        openBox(e) {
            this.isOpen = true
            box.style.display = 'block'
            body.style.overflowY = 'hidden'
            this.current = ([...imgs].indexOf(e.target))
            this.runSlider(true)
        }

        closeBox() {
            box.style.display = 'none'
            body.style.overflowY = 'scroll'
            this.isOpen = false
        }

        nextSlide() {
            if(this.current === imgs.length - 1) return
            this.current++
            this.runSlider(false)
        }

        prevSlide() {
            if(this.current === 0) return
            this.current = this.current - 1
            this.runSlider(false)
        }

        runSlider(immediately) {
            const duration = immediately ? 0 : 0.4
            gsap.to(document.querySelector('.box__mover'), {duration: duration, x: `${-100 * this.current}vw`})
        }
    }

    const slider = new Slider()

    imgs.forEach(el => {
        el.addEventListener('click', (e) => slider.openBox(e))
    })


    nextBtn.addEventListener('click', () => slider.nextSlide())
    prevBtn.addEventListener('click', () => slider.prevSlide())
    closeBtn.addEventListener('click', () => slider.closeBox())

//     //HAMMER
    const stage = document.querySelector('.box');

    const hammertime = new Hammer(stage);

    hammertime.on('swipe', ({velocityX}) => {
        if(velocityX > 0) {
            slider.prevSlide();
        } else {
            slider.nextSlide();
        }
    });
// }
}

export default slider