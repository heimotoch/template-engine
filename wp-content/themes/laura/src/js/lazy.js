import gsap from 'gsap'

const lazy = () => {
    const images = document.querySelectorAll('.images__image')

    const bumpImgs = (resize = false) => {
        images.forEach((img,i) => {
                if(!i) return;
                const prevImg = img.previousElementSibling
            
                if(!prevImg) return;
                const bumpVal = img.dataset.bump
                const bumpValCalc = prevImg.offsetHeight * (+bumpVal / 100)
                       
                img.style.marginTop = -bumpValCalc + 'px'

                const parent = img.parentNode   
                const minHeight = parent.style.minHeight.replace('px', '')
                console.log(minHeight)

                if(minHeight < prevImg.offsetHeight || resize) {
                    img.parentNode.style.minHeight = prevImg.offsetHeight + 'px' 
                }              
      })
    }


    window.addEventListener('resize', () => bumpImgs(true));


        // setTimeout(() => {
        //     bumpImgs();
        // }, 1000)
        









    const enterAnim = (ev) => { 
        // const img = ev.target
        // // bumpImgs();
        // const prevImg = img.previousElementSibling
        
        // if(!prevImg) return;
        // const bumpVal = img.dataset.bump
        // const bumpValCalc = prevImg.offsetHeight * (+bumpVal / 100)
        
        
        // img.style.marginTop = -bumpValCalc + 'px'
        // img.style.paddingBottom = bumpValCalc - img.offsetHeight + 'px'



        // gsap.set(ev.target, {y: 100, opacity: 0 })
        bumpImgs();
        gsap.to(ev.target, {y: 0, opacity: 1 })
    }

    const teamContent = document.querySelectorAll('.img_anim');

    let optionsEnter = {
        rootMargin: "0px",
        threshold: 0.25
    }

    let observerEnter = new IntersectionObserver(entries => {
        entries.forEach(enter => {
            if (enter.intersectionRatio > 0.25 && ( enter.boundingClientRect.top > 0 || enter.boundingClientRect.bottom > 0)) {
                enterAnim(enter)
            }
        })
    }, optionsEnter);


    teamContent.forEach(el => {
        observerEnter.observe(el);
    })

}

export default lazy